# Decentralized Camp Workshop 1

We will be using [Space.sh](https://space.sh), make sure you install it.

## Pull Docker image from Space.sh
```sh
docker pull registry.gitlab.com/space-sh/ethereum
```  

## Run the container in daemon mode
```sh
docker run --name geth -d -p 8080:80 -p 8545:8545 registry.gitlab.com/space-sh/ethereum tail -f /dev/null
```  
The `tail -f /dev/null` is just to keep the container alive by sleeping indefinitely.

## Setup web server and MyEtherWallet
```sh
#
# Enter into container (Alpine Linux):

space -m docker /enter/ -- geth

apk update && apk add lighttpd

echo "My very own web server is running" > /var/www/localhost/htdocs/index.html

lighttpd -f /etc/lighttpd/lighttpd.conf

curl 127.0.0.1

#
# Setup MyEtherWallet
cd /var/www/localhost/htdocs/
curl -L -O https://github.com/kvhnuke/etherwallet/releases/download/v3.10.4.1/etherwallet-v3.10.4.1.zip
unzip etherwallet-v3.10.4.1.zip
mv etherwallet-v3.10.4.1/* .
```

## Run Geth node in development mode
Still inside the same shell as above:  

```sh
#
# WARNING: This is a very unsecure setup to run since it exposes everything.
geth --dev --rpc --rpcapi="miner,db,eth,net,personal,web3" --rpcaddr "0.0.0.0" --rpccorsdomain "*"  console 2>>/tmp/geth.log

> personal.newAccount()
> miner.setEtherbase('0x...')
> miner.start()
> miner.stop()
> eth.getBalance('0x...')

```  

We now have a Geth node setup and some ether in the `etherbase` account.

## Compile contract, deploy and interact using curl

We need to copy the `hello.sol` Solidity contract into our container so we can use the compiler which is pre installed into the container.

We use `Space.sh` to help us with this.  

From another terminal window:  
```sh
# copy hello.sol into container.
cat hello.sol | space -m file /pipewrite/ -m docker /wrap_exec/ -e DOCKERCONTAINER=geth -e file=/home/ethereum-dev/hello.sol

# Enter the container again:  
space -m docker /enter/ -- geth

#
# Compile contract
cd /home/ethereum-dev/
solc hello.sol -o . --bin
solc hello.sol -o . --abi

data="0x"$(cat hello.bin)

# Set etherbase account
from="0x..."

# Tell Geth to unlock the account
curl -X POST --data '{"jsonrpc":"2.0","method":"personal_unlockAccount","params":["'${from}'", "abc"], "id":0}' localhost:8545

curl --data '{"jsonrpc":"2.0","method": "eth_sendTransaction", "params": [{"from": "'${from}'", "gas": "0x72BF0", "data": "'${data}'"}], "id": 0}' localhost:8545

# Node must be mining.

receipt="0x..."
curl --data '{"jsonrpc":"2.0","method": "eth_getTransactionReceipt", "params": ["'${receipt}'"], "id": 0}' localhost:8545

contract_address="0x.."

fnsum=$(printf "greet()" | keccak-256sum)
fn="0x${fnsum:0:8}"

curl -X POST --data '{"jsonrpc":"2.0","method":"eth_call", "params": [{"from": "'${from}'", "to": "'${contract_address}'", "gas": "0x72BF0", "data": "'${fn}'"}, "latest" ], "id": 0}' localhost:8545

# Read the result
result="0x..."
echo "${result}" | xxd -r -p ; echo
```


## Deploy and interact with contract using Geth and web3.js

### Enter geth console
```sh
geth attach http://:8545 console
```

### store hello.abi and hello.bin on contract variables

```js
var abi='[{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"greet","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_greeting","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"}]';

var contract_abi=JSON.parse(abi);

var contract_bin="0x...<insert hello.bin data here>"
```

### Deploy contract
```js
personal.unlockAccount(eth.accounts[0]);
var contract = eth.contract(contract_abi);
var contract_instance = contract.new("Hello World", { from: eth.accounts[0], data: contract_bin, gas: 470000});
```

### Using a callback to watch for contract deployment process
```js
var contract_instance = contract.new("Hello World", { from: eth.accounts[0], data: contract_bin, gas: 470000},  function (e, contract){console.log(e, contract); if (typeof contract.address !== 'undefined') { console.log("Contract deployed: " + contract.address);}});
```

### Access data

##### Read transaction hash
```js
contract_instance.transactionHash
```
Output:
```
"0x2d6614588d3224079091b9dabbab2bc855c3e3594c1022a8cfb1458a85326adc"
```

##### Read contract address
```js
miner.start()

contract_instance.address
```

In case the contract hasn't been mined yet:
```
"undefined"
```

Remember it is necessary to mine the transaction so the contract gets deployed:
```js
miner.start()
```

Now, `contract_instance.address` should output an address similar to the following one:
```sh
"0x79b4c43f9b9db7920d7805927d7d3a125ea69618"
```


### Accessing the contract
```js
contract.at(contract_instance.address);
```

### Calling the greet() function:
```js
contract.at(contract_instance.address).greet();
```

Output:
```sh
"Hello World"
```

With that, it is possible to run any other contract, provided the interface is public and the address is known. 
The calling construct is the same:
```js
contract = eth.contract(someone_else_contract_abi);
contract_instance = contract.at(someone_else_contract_address);
contract_instance.SomeFunction();
```

## Access MyEtherWallet

* Open browser to http://127.0.0.1:8080
* On the top right corner, add a new custom node and fill up fields accordingly:
* Node Name: Blockie node
* URL: http://127.0.0.1
* Port: 8545
* Select Custom
* Click Save and Use custom node
* Create a new wallet and take note of private&public keys


## Fund new account from etherbase account

```sh
space -m docker /enter/ -- geth

> personal.unlockAccount("0x...")
> eth.sendTransaction({from:"0x...", to:"0xMewWallet", value: web3.toWei(1, "ether"), gasPrice: 1})
> # Check Tx in MEW
> miner.start()
> miner.stop()
> # Check Tx in MEW

```

## Interact with contract from MyEtherWallet

The **Contracts** section makes it possible to interact with a given contract if one knows where it is located and what is the interface (ABI).  
For the hello contract, that information has been revealed once and stored in the `contract_address`  variable. Example:
```
0x667acde02da657aaae53a45383ff9f06418178bd
```

The **ABI** has already been saved in a previous step to `/home/ethereum-dev/hello.abi` file:
```
[{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"greet","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_greeting","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"}]
```

After filling up the previous fields, hit **Access** to proceed to the next step.  
It is now possible to perform reads and writes directly from the **MyEtherWallet** frontend.  
Let's try calling the `greet()` function by selecting it on the list.  
On success, it will return an *ASCII* string as result.

/TUTORIAL
