pragma solidity ^0.4.17;

// Simple contract based on ethereum.org/greeter
contract hello {
    /* Define variable owner of the type address */
    address owner;

    /* Define variable greeting of the type string */
    string greeting;

    /* This runs when the contract is executed */
    function hello(string _greeting) public {
        owner = msg.sender;
        greeting = _greeting;
        if(bytes(greeting).length == 0) {
            greeting = "Hello from Space";
        }
    }

    /* Main function */
    function greet() constant public returns (string) {
        return greeting;
    }

    /* Function to recover the funds on the contract */
    function kill() public { if (msg.sender == owner) selfdestruct(owner); }
}
